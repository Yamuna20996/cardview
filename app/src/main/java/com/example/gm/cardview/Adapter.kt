package com.example.gm.cardview

import android.content.Context
import android.icu.lang.UCharacter.GraphemeClusterBreak.V
import android.support.constraint.R.id.parent
import android.support.v7.view.menu.MenuView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.widget.Toast



class Adapter(val items : ArrayList<Movie>, val context: Context) : RecyclerView.Adapter<Adapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Adapter.ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.listitem, p0, false)
        v.setOnClickListener {
            Toast.makeText(context, "cardViewClicked", Toast.LENGTH_LONG).show()
            var itr:Intent=Intent(context,SecondActivity::class.java)

            context.startActivity(itr.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        }
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: Adapter.ViewHolder, p1: Int) {
        p0.bindItems(items[p1])

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(movie: Movie) {

            val iron = itemView.findViewById(R.id.iron) as ImageView
            val textiron = itemView.findViewById(R.id.textiron) as TextView
            val india: TextView = itemView.findViewById(R.id.india) as TextView
            val film: TextView = itemView.findViewById(R.id.film) as TextView
            val description: TextView = itemView.findViewById(R.id.description) as TextView
            val video: TextView = itemView.findViewById(R.id.video) as TextView
            val casting: TextView = itemView.findViewById(R.id.casting) as TextView
            val rating: TextView = itemView.findViewById(R.id.ratings) as TextView
            val photo: TextView = itemView.findViewById(R.id.photo) as TextView
            val star: ImageView = itemView.findViewById(R.id.star) as ImageView

            iron.setImageResource(R.drawable.iron)
            textiron.text = movie.irontext
            india.text = movie.india
            film.text = movie.film
            description.text = movie.description
            video.text = movie.video
            casting.text = movie.casting
            rating.text = movie.rating
            photo.text = movie.photo
            star.setImageResource(R.drawable.staricon)

        }
    }
}
