package com.example.gm.cardview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import kotlinx.android.synthetic.main.second_activity.*
import kotlinx.android.synthetic.main.toolbar.*

class SecondActivity :AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)
        setSupportActionBar(tool)

        tool.title="Back"
        tool.setLogo(R.drawable.ic_arrow_back_black_24dp)

}

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.item,menu)
        return true
    }
}