package com.example.gm.cardview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val list=ArrayList<Movie>()

        list.add(Movie(R.id.iron,"IronMan","India","Film","After being kidnapped by a powerful terrorist organization, brilliant industrialist\n" +
                "        Tony Stark narrowly escapes using an iron suit he crafted from scrap metal and spare parts",
            "Videography","Casting","Rating","Photography",R.id.star
            ))
        list.add(Movie(R.id.iron,"IronMan","India","Film","After being kidnapped by a powerful terrorist organization, brilliant industrialist\n" +
                "        Tony Stark narrowly escapes using an iron suit he crafted from scrap metal and spare parts",
            "Videography","Casting","Rating","Photography",R.id.star
        ))
        list.add(Movie(R.id.iron,"IronMan","India","Film","After being kidnapped by a powerful terrorist organization, brilliant industrialist\n" +
                "        Tony Stark narrowly escapes using an iron suit he crafted from scrap metal and spare parts",
            "Videography","Casting","Rating","Photography",R.id.star
        ))
        list.add(Movie(R.id.iron,"IronMan","India","Film","After being kidnapped by a powerful terrorist organization, brilliant industrialist\n" +
                "        Tony Stark narrowly escapes using an iron suit he crafted from scrap metal and spare parts",
            "Videography","Casting","Rating","Photography",R.id.star
        ))
        list.add(Movie(R.id.iron,"IronMan","India","Film","After being kidnapped by a powerful terrorist organization, brilliant industrialist\n" +
                "        Tony Stark narrowly escapes using an iron suit he crafted from scrap metal and spare parts",
            "Videography","Casting","Rating","Photography",R.id.star
        ))
        list.add(Movie(R.id.iron,"IronMan","India","Film","After being kidnapped by a powerful terrorist organization, brilliant industrialist\n" +
                "        Tony Stark narrowly escapes using an iron suit he crafted from scrap metal and spare parts",
            "Videography","Casting","Rating","Photography",R.id.star
        ))
        list.add(Movie(R.id.iron,"IronMan","India","Film","After being kidnapped by a powerful terrorist organization, brilliant industrialist\n" +
                "        Tony Stark narrowly escapes using an iron suit he crafted from scrap metal and spare parts",
            "Videography","Casting","Rating","Photography",R.id.star
        ))


        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager=LinearLayoutManager(this,LinearLayout.VERTICAL,false)


        val adapter1=Adapter(list, applicationContext)

        recyclerView.adapter=adapter1


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.values,menu)
        return true
    }

//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        val id=item!!.itemId
//
//        if (id==R.id.add){
//            return true
//        }else{
//            return true
//        }
//        return super.onOptionsItemSelected(item)
//    }
}
